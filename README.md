manjaroopenboxfrench
====================

Description
----

Des fichiers de configuration francisés pour Manjaro Openbox.


Instructions pour la configuration de Conky :
----

## Installation

1. copier le fichier .conkyrc_french dans ~/
2. renommer le fichier .conkyrc initialement présent en .conkyrc.old
3. renommer le fichier .conkyrc_french en .conkyrc
4. Se déconnecter
5. Se reconnecter

## Désinstallation

1. Supprimer le fichier .conkyrc
2. Renommer le fichier .conkyrc.old en .conkyrc
3. Se déconnecter
4. Se reconnecter


Instructions pour la configuration d'obmenu-generator :
----

## Installation

1. copier le fichier schema.pl_french dans ~/.config/obmenu-generator/

2. éditer le fichier schema.pl_french pour remplacer le motif «
   remplacerparlenomdutilisateur » de la ligne « require
   '/home/remplacerparlenomdutilisateur/.config/obmenu-generator/config.pl';
   » par votre nom d'utilisateur.

3. renommer le fichier schema.pl initialement présent en schema.pl.old

4. renommer le fichier schema.pl_french en schema.pl

## Désinstallation

1. Supprimer le fichier schema.pl
2. Renommer le fichier schema.pl.old en schema.pl
